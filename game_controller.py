from time import sleep
import pygame

from game.playfield import Playfield

block_size = 48
blockcount_horizontal = 15
blockcount_vertical = 9

class Game_Controller():
    def __init__(self):
        self.field = None
        self.active = False
        self.players = 2

    def start(self):
        self.field = Playfield(blockcount_horizontal, blockcount_vertical, block_size, self.players)
        self.active = True

    def stop(self):
        self.active = False
        sleep(2)

    def set_players(self, value, players):
        self.players = players
        
    def frame(self, screen):
        if len(self.field.player_group) <= 1:
            self.stop()

        pressed_keys = pygame.key.get_pressed()
        for player in self.field.player_group.sprites():
            player.act(pressed_keys, self.field)

        self.field.player_group.update()
        self.field.bomb_group.update()
        self.field.fire_group.update(self.field)
        self.field.crate_group.update()
        self.field.upgrade_group.update(self.field)

        self.field.grass_group.draw(screen)
        self.field.wall_group_outer.draw(screen)
        self.field.wall_group_inner.draw(screen)
        self.field.upgrade_group.draw(screen)
        self.field.crate_group.draw(screen)
        self.field.bomb_group.draw(screen)
        self.field.player_group.draw(screen)
        self.field.fire_group.draw(screen)