import pygame
import pygame_menu
from sys import exit

from game_controller import Game_Controller

block_size = 48
blockcount_horizontal = 15
blockcount_vertical = 9
screen_width = (blockcount_horizontal+2) * block_size
screen_height = (blockcount_vertical+2) * block_size

game = Game_Controller()

def main():
    pygame.init()

    screen_size = (screen_width, screen_height)
    screen = pygame.display.set_mode(screen_size)
    pygame.display.set_caption("Bomberman")
    clock = pygame.time.Clock()

    menu_test = pygame_menu.Menu("Menu", screen_width, screen_height)
    menu_test.add.button("Play", game.start)
    menu_test.add.selector("Players", [("Two", 2), ("Three", 3), ("Four", 4)], onchange=game.set_players)
    menu_test.add.button("Quit", pygame_menu.events.EXIT)

    #Main Game-Loop
    while True:
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                exit()

        if game.active:
            game.frame(screen)
        else:
            menu_test.update(events)
            menu_test.draw(screen)

        pygame.display.update()
        clock.tick(60)

if __name__ == "__main__":
    main()