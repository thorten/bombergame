import pygame

from game_resource import Game_Resource

class Wall(pygame.sprite.Sprite):

    def __init__(self, rect, wall_id):
        super().__init__()
        self.rect = rect
        if wall_id == 1:
            self.image = Game_Resource.getInstance().wall1
        else:
            self.image = Game_Resource.getInstance().wall2