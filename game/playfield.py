import pygame
import math
import random

from game.bomb import Bomb
from game.crate import Crate
from game.fire import Fire
from game.grass import Grass
from game.player import Player
from game.upgrade import Upgrade
from game.wall import Wall
from game.sprite_group import Sprite_Group, Init_Sprite_Group

class Playfield:

    def __init__(self, size_horizontal, size_vertical, block_size, num_players=2):
        """Constructor for a Playfield."""
        self.size_horizontal = size_horizontal
        self.size_vertical = size_vertical
        self.block_size = block_size

        self.bomb_group = Sprite_Group()
        self.fire_group = Sprite_Group()
        self.place_grass()
        self.place_walls()
        self.generate_crates()
        self.generate_upgrades()
        self.place_players(num_players)

    def place_grass(self):
        """Places the grass."""
        grasses = []
        for i in range(self.size_vertical+2):
            for j in range(self.size_horizontal+2):
                grass_pos_x = self.block_size * j
                grass_pos_y = self.block_size * i
                grass_rect = pygame.rect.Rect(grass_pos_x, grass_pos_y, self.block_size, self.block_size)
                grass_is_junction = (j % 2 != 0 and i % 2 != 0)
                grasses.append(Grass(grass_rect, grass_is_junction))
        self.grass_group = Init_Sprite_Group(grasses)

    def place_walls(self):
        """Places the walls."""
        walls = []
        #add horizontal border walls
        for i in range(self.size_horizontal+2):
            wall_pos_x = self.block_size * i
            wall_pos_y = 0
            wall_rect = pygame.rect.Rect(wall_pos_x, wall_pos_y, self.block_size, self.block_size)
            walls.append(Wall(wall_rect, 1))
            wall_pos_y = self.block_size * (self.size_vertical + 1)
            wall_rect = pygame.rect.Rect(wall_pos_x, wall_pos_y, self.block_size, self.block_size)
            walls.append(Wall(wall_rect, 1))

        #add vertical border walls
        for i in range(self.size_vertical+1):
            wall_pos_y = self.block_size * i
            wall_pos_x = 0
            wall_rect = pygame.rect.Rect(wall_pos_x, wall_pos_y, self.block_size, self.block_size)
            walls.append(Wall(wall_rect, 2))
            wall_pos_x = self.block_size * (self.size_horizontal + 1)
            wall_rect = pygame.rect.Rect(wall_pos_x, wall_pos_y, self.block_size, self.block_size)
            walls.append(Wall(wall_rect, 2))

        self.wall_group_outer = Init_Sprite_Group(walls)

        walls = []
        #add the inner walls
        for i in range(self.size_vertical):
            for j in range(self.size_horizontal):
                if i % 2 == 0 or j % 2 == 0:
                    continue
                wall_pos_x = self.block_size + self.block_size * j
                wall_pos_y = self.block_size + self.block_size * i
                wall_rect = pygame.rect.Rect(wall_pos_x, wall_pos_y, self.block_size, self.block_size)
                walls.append(Wall(wall_rect, 1))

        self.wall_group_inner = Init_Sprite_Group(walls)
        

    def generate_crates(self):
        """Generates the positions of the crates."""
        crates = []
        size = self.size_horizontal * self.size_vertical
        low_range = int(size * 0.3)
        high_range = int(size * 0.34)
        crate_num = random.randint(low_range, high_range)
        possible_pos = []
        start_coords_x = [0, 1, self.size_vertical-1, self.size_vertical-2]
        start_coords_y = [0, 1, self.size_horizontal-1, self.size_horizontal-2]
        for i in range(self.size_vertical):
            for j in range(self.size_horizontal):
                if i in start_coords_x and j in start_coords_y:                     #Corners of the Field, where the players start
                    continue
                if i % 2 != 0 and j % 2 != 0:                                       #Positions of indestructable walls
                    continue
                possible_pos.append((j, i))
        
        crate_pos = []
        for i in range(crate_num):
            pos_num = len(possible_pos)
            pos = random.randrange(pos_num)
            crate_pos_x = self.block_size + self.block_size * possible_pos[pos][0]
            crate_pos_y = self.block_size + self.block_size * possible_pos[pos][1]
            crate_rect = pygame.rect.Rect(crate_pos_x, crate_pos_y, self.block_size, self.block_size)
            crates.append(Crate(crate_rect))

        self.crate_group = Init_Sprite_Group(crates)

    def generate_upgrades(self):
        """Generates the positions and type of the upgrades, that are under the crates."""
        upgrades = []
        crate_num = len(self.crate_group)
        low_range = int(crate_num * 0.1)
        high_range = int(crate_num * 0.3)
        upgrade_num = random.randint(low_range, high_range)
        possible_crates = []
        for crate in self.crate_group:
            possible_crates.append(crate)
        for i in range(upgrade_num):
            upgrade_type = random.randint(1, 3)
            crate_index = random.randrange(len(possible_crates))
            upgrade_rect = pygame.rect.Rect(0, 0, 32, 32)
            upgrade_rect.center = possible_crates[crate_index].rect.center
            upgrades.append(Upgrade(upgrade_rect, upgrade_type))
            possible_crates.pop(crate_index)

        self.upgrade_group = Init_Sprite_Group(upgrades)

    def place_players(self, num_players):
        """Places players in the cornes of the playfield."""
        players = []
        for i in range(num_players):
            player_start_pos_x = (i % 2) * (self.size_horizontal-1) * self.block_size + self.block_size
            player_start_pos_y = int(i / 2) * (self.size_vertical-1) * self.block_size + self.block_size
            rect = pygame.Rect(player_start_pos_x, player_start_pos_y, self.block_size, self.block_size)
            player = Player(i, rect)
            players.append(player)
        
        self.player_group = Init_Sprite_Group(players)


    def place_bomb(self, player):
        """Places a bomb in order of a player."""
        player_pos_x = player.rect.left
        player_pos_y = player.rect.top

        possible_places = pygame.sprite.spritecollide(player, self.grass_group, False)
        nearest_place = player.rect
        nearest_dist = float('inf')
        for possible_place in possible_places:
            place_pos_x = possible_place.rect.left
            place_pos_y = possible_place.rect.top
            delta_x = abs( player_pos_x - place_pos_x )
            delta_y = abs( player_pos_y - place_pos_y )
            dist = math.hypot(delta_x, delta_y)

            if dist < nearest_dist:
                nearest_dist = dist
                nearest_place = possible_place

        bomb_rect = nearest_place.rect
        self.bomb_group.add(Bomb(bomb_rect, player, self))
    
    def place_fire(self, bomb):
        """Places fire, that comes from a exploding bomb."""
        fires = []
        fires.append(Fire(bomb.rect, 3))
        fires_up = []
        fires_down = []
        fires_left = []
        fires_right = []
        for i in range(bomb.range-1):
            dist = self.block_size * i + self.block_size
            fire_up_rect = pygame.rect.Rect(bomb.rect.left, bomb.rect.top-dist, self.block_size, self.block_size)
            fires_up.append(Fire(fire_up_rect, 6))
            fire_down_rect = pygame.rect.Rect(bomb.rect.left, bomb.rect.top+dist, self.block_size, self.block_size)
            fires_down.append(Fire(fire_down_rect, 6))
            fire_left_rect = pygame.rect.Rect(bomb.rect.left-dist, bomb.rect.top, self.block_size, self.block_size)
            fires_left.append(Fire(fire_left_rect, 2))
            fire_right_rect = pygame.rect.Rect(bomb.rect.left+dist, bomb.rect.top, self.block_size, self.block_size)
            fires_right.append(Fire(fire_right_rect, 2))

        dist = self.block_size * bomb.range
        fire_up_rect = pygame.rect.Rect(bomb.rect.left, bomb.rect.top-dist, self.block_size, self.block_size)
        fires_up.append(Fire(fire_up_rect, 5))
        fire_down_rect = pygame.rect.Rect(bomb.rect.left, bomb.rect.top+dist, self.block_size, self.block_size)
        fires_down.append(Fire(fire_down_rect, 7))
        fire_left_rect = pygame.rect.Rect(bomb.rect.left-dist, bomb.rect.top, self.block_size, self.block_size)
        fires_left.append(Fire(fire_left_rect, 1))
        fire_right_rect = pygame.rect.Rect(bomb.rect.left+dist, bomb.rect.top, self.block_size, self.block_size)
        fires_right.append(Fire(fire_right_rect, 4))

        fire_directions = [fires_up, fires_down, fires_left, fires_right]
        for fire_direction in fire_directions:
            for fire in fire_direction:
                if pygame.sprite.spritecollideany(fire, self.wall_group_outer):
                    break
                if pygame.sprite.spritecollideany(fire, self.wall_group_inner):
                    break
                collided_crates = pygame.sprite.spritecollide(fire, self.crate_group, False)
                for collided_crate in collided_crates:
                    collided_crate.is_burning = True               #crate destruction
                if collided_crates:   
                    break
                fires.append(fire)

        self.fire_group.add(fires)
