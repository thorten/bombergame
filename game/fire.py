import pygame

from game_resource import Game_Resource

FIRE_TIME = 30

class Fire(pygame.sprite.Sprite):

    def __init__(self, rect, fire_id):
        super().__init__()
        self.rect = rect
        if(fire_id == 1): 
            self.image = Game_Resource.getInstance().fire_left
        elif(fire_id == 2): 
            self.image = Game_Resource.getInstance().fire_horizontal
        elif(fire_id == 3): 
            self.image = Game_Resource.getInstance().fire_center
        elif(fire_id == 4): 
            self.image = Game_Resource.getInstance().fire_right
        elif(fire_id == 5): 
            self.image = Game_Resource.getInstance().fire_up
        elif(fire_id == 6): 
            self.image = Game_Resource.getInstance().fire_vertical
        else: 
            self.image = Game_Resource.getInstance().fire_down

        self.time = FIRE_TIME

    def update(self, field):
        self.time -= 1
        if self.time <= 0:
            self.kill()

        players = pygame.sprite.spritecollide(self, field.player_group, False)
        for player in players:
            player.fire_touched(self)
        bombs = pygame.sprite.spritecollide(self, field.bomb_group, False)
        for bomb in bombs:
            bomb.time = 0