import pygame

from game_resource import Game_Resource

class Grass(pygame.sprite.Sprite):

    def __init__(self, rect, is_junction):
        super().__init__()
        self.rect = rect
        self.image = Game_Resource.getInstance().grass
        self.is_junction = is_junction