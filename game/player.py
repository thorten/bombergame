import math
import pygame

from game_resource import Game_Resource

PLAYER_CONTROLS = [[pygame.K_w, pygame.K_s, pygame.K_a, pygame.K_d, pygame.K_e],                        #player1 wasd + e
                    [pygame.K_UP, pygame.K_DOWN, pygame.K_LEFT, pygame.K_RIGHT, pygame.K_KP0],          #player2 arrow_keys + 0 (NUMPAD)
                    [pygame.K_i, pygame.K_k, pygame.K_j, pygame.K_l, pygame.K_o],                       #player3 ijkl + o
                    [pygame.K_t, pygame.K_g, pygame.K_f, pygame.K_h, pygame.K_z]]                       #player4 tgfh + z

SUBPOS_MAX = 3
ANIMATION_TIME = 16 * SUBPOS_MAX

class Player(pygame.sprite.Sprite):

    def __init__(self, player_id, rect):
        """Constructor for a Player."""
        super().__init__()
        self.player_id = player_id
        self.image = Game_Resource.getInstance().player[player_id]["front"]["stand"]
        self.image_old = self.image
        self.rect = rect
        self.rect_old = self.rect
        self.subPosX = 0
        self.subPosY = 0
        
        controls = PLAYER_CONTROLS[player_id]
        self.up_button = controls[0]
        self.down_button = controls[1]
        self.left_button = controls[2]
        self.right_button = controls[3]
        self.bomb_button = controls[4]
        self.bomb_button_released = True  

        self.placed_bombs = 0
        self.speed = 1 * (SUBPOS_MAX + 1)
        self.bombs = 1
        self.range = 1

        self.time = ANIMATION_TIME
        self.state_old = "stand"

    def act(self, pressed_keys, field):
        """The player does an action, based on the button input."""
        self.rect_old = self.rect
        self.image_old = self.image

        #place bomb if button is pressed and released and the player has not placed all bombs yet
        if pressed_keys[self.bomb_button] and self.bomb_button_released and self.placed_bombs < self.bombs:
            self.bomb_button_released = False
            field.place_bomb(self)
            self.placed_bombs += 1
        if not pressed_keys[self.bomb_button]:
            self.bomb_button_released = True

        #change subposition if a direction button is pressed
        if pressed_keys[self.up_button]:
            self.subPosY -= self.speed
        elif pressed_keys[self.down_button]:
            self.subPosY += self.speed
        elif pressed_keys[self.left_button]:
            self.subPosX -= self.speed
        elif pressed_keys[self.right_button]:
            self.subPosX += self.speed
        
        self.move_player()

        #can't walk inside outer walls and crates
        if pygame.sprite.spritecollideany(self, field.wall_group_outer) or pygame.sprite.spritecollideany(self, field.crate_group):
            self.rect = self.rect_old

        #cant' walk inside bombs, but is able to walk outside of them
        if pygame.sprite.spritecollideany(self, field.bomb_group):
            temp_rect = self.rect
            self.rect = self.rect_old
            if pygame.sprite.spritecollideany(self, field.bomb_group):
                self.rect = temp_rect

        #can't walk inside inner walls, instead takes a step to the next junction
        if pygame.sprite.spritecollideany(self, field.wall_group_inner):
            possible_junctions = pygame.sprite.spritecollide(self, field.grass_group, False)
            junction = None
            for possible_junction in possible_junctions:
                if possible_junction.is_junction:
                    junction = possible_junction
                    break

            if not junction:
                self.rect = self.rect_old
            else:
                direction_x = junction.rect.left - self.rect_old.left
                direction_y = junction.rect.top - self.rect_old.top

                #move the player to the junction
                if direction_x > 0:
                    self.subPosX += self.speed
                elif direction_x < 0:
                    self.subPosX -= self.speed
                if direction_y > 0:
                    self.subPosY += self.speed
                elif direction_y < 0:
                    self.subPosY -= self.speed

                self.move_player()

                #correct position if the player would have walked over the junction
                if direction_x > 0 and self.rect.left > junction.rect.left:
                    self.rect.left = junction.rect.left
                elif direction_x < 0 and self.rect.left < junction.rect.left:
                    self.rect.left = junction.rect.left
                if direction_y > 0 and self.rect.top > junction.rect.top:
                    self.rect.top = junction.rect.top
                elif direction_y < 0 and self.rect.top < junction.rect.top:
                    self.rect.top = junction.rect.top

    def move_player(self):
        """Moves the player in a direction."""
        distance = 0
        if self.subPosX > SUBPOS_MAX:
            self.rect = self.rect.move((self.subPosX / SUBPOS_MAX, 0))
            self.subPosX = self.subPosX % SUBPOS_MAX
        elif self.subPosX < -SUBPOS_MAX:
            self.rect = self.rect.move((self.subPosX / SUBPOS_MAX, 0))
            self.subPosX = (self.subPosX % SUBPOS_MAX) * -1
        if self.subPosY > SUBPOS_MAX:
            self.rect = self.rect.move((0, self.subPosY / SUBPOS_MAX))
            self.subPosY = self.subPosY % SUBPOS_MAX
        elif self.subPosY < -SUBPOS_MAX:
            self.rect = self.rect.move((0, self.subPosY / SUBPOS_MAX))
            self.subPosY = (self.subPosY % SUBPOS_MAX) * -1
    
    def fire_touched(self, fire):
        """If a player is in fire with half of his sprite, he is killed."""
        pos_x = self.rect.left
        pos_y = self.rect.top
        fire_pos_x = fire.rect.left
        fire_pos_y = fire.rect.top
        delta_x = abs( pos_x - fire_pos_x )
        delta_y = abs( pos_y - fire_pos_y )
        distance = math.hypot(delta_x, delta_y)
        if distance <= (self.rect.width / 2):
            self.kill()

    def get_upgrade(self, upgrade):
        """Player gets an upgrade."""
        if upgrade.upgrade_id == 1:
            self.bombs += 1
        elif upgrade.upgrade_id == 2:
            self.range += 1
        elif upgrade.upgrade_id == 3:
            self.speed += 1

    def update(self):
        """Handles the animation of the player."""
        self.time -= self.speed
        if self.time <= 0:
            self.time = ANIMATION_TIME
            self.image_old = self.image
            res = Game_Resource.getInstance()

            #Player stands still
            if self.rect == self.rect_old:
                if self.image_old in res.player[self.player_id]["front"].values():
                    self.image = res.player[self.player_id]["front"]["stand"]
                elif self.image_old in res.player[self.player_id]["back"].values():
                    self.image = res.player[self.player_id]["back"]["stand"]
                elif self.image_old in res.player[self.player_id]["left"].values():
                    self.image = res.player[self.player_id]["left"]["stand"]
                elif self.image_old in res.player[self.player_id]["right"].values():
                    self.image = res.player[self.player_id]["right"]["stand"]
                self.state_old = "stand"
            #player moves
            else:
                #move down
                if self.rect.top > self.rect_old.top:
                    if self.state_old == "walk1":
                        self.image = res.player[self.player_id]["front"]["walk2"]
                        self.state_old = "walk2"
                    else:
                        self.image = res.player[self.player_id]["front"]["walk1"]
                        self.state_old = "walk1"
                #move up
                elif self.rect.top < self.rect_old.top:
                    if self.state_old == "walk1":
                        self.image = res.player[self.player_id]["back"]["walk2"]
                        self.state_old = "walk2"
                    else:
                        self.image = res.player[self.player_id]["back"]["walk1"]
                        self.state_old = "walk1"
                #move left
                elif self.rect.left < self.rect_old.left:
                    if self.state_old == "walk2":
                        self.image = res.player[self.player_id]["left"]["walk3"]
                        self.state_old = "walk3"
                    elif self.state_old == "walk1":
                        self.image = res.player[self.player_id]["left"]["walk2"]
                        self.state_old = "walk2"
                    else:
                        self.image = res.player[self.player_id]["left"]["walk1"]
                        self.state_old = "walk1"
                #move right
                elif self.rect.left > self.rect_old.left:
                    if self.state_old == "walk2":
                        self.image = res.player[self.player_id]["right"]["walk3"]
                        self.state_old = "walk3"
                    elif self.state_old == "walk1":
                        self.image = res.player[self.player_id]["right"]["walk2"]
                        self.state_old = "walk2"
                    else:
                        self.image = res.player[self.player_id]["right"]["walk1"]
                        self.state_old = "walk1"