import pygame

class Sprite_Group(pygame.sprite.Group):
    def __init__(self):
        """Group that is initiated without sprites."""
        super().__init__()

class Init_Sprite_Group(pygame.sprite.Group):
    def __init__(self, sprites):
        """Group that is initiated with every sprite it contains."""
        super().__init__(sprites)