import pygame

from game_resource import Game_Resource

BOMB_TIME = 3 * 60

class Bomb(pygame.sprite.Sprite):

    def __init__(self, rect, player, field):
        super().__init__()
        self.rect = rect
        self.time = BOMB_TIME
        self.image = Game_Resource.getInstance().bomb1

        self.range = player.range
        self.player = player
        self.field = field
    
    def update(self):
        self.time -= 1

        if self.time == int(BOMB_TIME*(2/3)):
            self.image = Game_Resource.getInstance().bomb2
        if self.time == int(BOMB_TIME*(1/3)):
            self.image = Game_Resource.getInstance().bomb3

        if self.time <= 0:
            self.player.placed_bombs -= 1
            self.field.place_fire(self)
            self.kill()