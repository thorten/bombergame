import pygame

from game_resource import Game_Resource

class Upgrade(pygame.sprite.Sprite):

    def __init__(self, rect, upgrade_id):
        """Constructor for an Upgrade."""
        super().__init__()
        self.rect = rect
        self.upgrade_id = upgrade_id
        #the image is set depending on the upgrade_id
        if upgrade_id == 1:
            self.image = Game_Resource.getInstance().upgrade_bomb
        elif upgrade_id == 2:
            self.image = Game_Resource.getInstance().upgrade_range
        else: #upgrade_id == 3
            self.image = Game_Resource.getInstance().upgrade_speed
    
    def update(self, field):
        """Checks if the upgrade is collected by a player."""
        player = pygame.sprite.spritecollideany(self, field.player_group)
        if player:  #if the upgrade is collected
            player.get_upgrade(self)
            self.kill()
        else:   #if the upgrade is destroyed by a bomb
            if pygame.sprite.spritecollideany(self, field.fire_group):
                self.kill()