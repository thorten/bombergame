import pygame

from game_resource import Game_Resource

BURN_TIME = 30

class Crate(pygame.sprite.Sprite):

    def __init__(self, rect):
        super().__init__()
        self.rect = rect
        self.image = Game_Resource.getInstance().crate
        self.time = BURN_TIME
        self.is_burning = False
    
    def update(self):
        if self.is_burning:
            self.time -= 1

            if self.time <= 0:
                self.kill()

