import pygame

class Game_Resource:
    class __Resource:
        def __init__(self):
            image_all = pygame.image.load("resources/bomb_party_v4.png").convert_alpha()
            image_all = pygame.transform.scale(image_all, (image_all.get_width() * 3, image_all.get_height() * 3))
            px = 16 * 3

            self.player = []
            for i in range(4):
                player_dict = {"front": {}, "back": {}, "right": {}, "left": {}}
                row = 14 + i

                player_dict["front"]["stand"] = image_all.subsurface(pygame.rect.Rect(1 * px, row * px, px, px)).convert_alpha()
                player_dict["front"]["walk1"] = image_all.subsurface(pygame.rect.Rect(2 * px, row * px, px, px)).convert_alpha()
                player_dict["front"]["walk2"] = image_all.subsurface(pygame.rect.Rect(3 * px, row * px, px, px)).convert_alpha()

                player_dict["back"]["stand"] = image_all.subsurface(pygame.rect.Rect(0 * px, row * px, px, px)).convert_alpha()
                player_dict["back"]["walk1"] = image_all.subsurface(pygame.rect.Rect(8 * px, row * px, px, px)).convert_alpha()
                player_dict["back"]["walk2"] = image_all.subsurface(pygame.rect.Rect(9 * px, row * px, px, px)).convert_alpha()

                player_dict["right"]["stand"] = image_all.subsurface(pygame.rect.Rect(4 * px, row * px, px, px)).convert_alpha()
                player_dict["right"]["walk1"] = image_all.subsurface(pygame.rect.Rect(5 * px, row * px, px, px)).convert_alpha()
                player_dict["right"]["walk2"] = image_all.subsurface(pygame.rect.Rect(6 * px, row * px, px, px)).convert_alpha()
                player_dict["right"]["walk3"] = image_all.subsurface(pygame.rect.Rect(7 * px, row * px, px, px)).convert_alpha()

                player_dict["left"]["stand"] = pygame.transform.flip(player_dict["right"]["stand"], True, False)
                player_dict["left"]["walk1"] = pygame.transform.flip(player_dict["right"]["walk1"], True, False)
                player_dict["left"]["walk2"] = pygame.transform.flip(player_dict["right"]["walk2"], True, False)
                player_dict["left"]["walk3"] = pygame.transform.flip(player_dict["right"]["walk3"], True, False)

                self.player.append(player_dict)

            self.grass = image_all.subsurface(pygame.rect.Rect(1 * px, 13 * px, px, px))
            self.crate = image_all.subsurface(pygame.rect.Rect(9 * px, 13 * px, px, px))

            self.wall1 = image_all.subsurface(pygame.rect.Rect(10 * px, 17 * px, px, px))
            self.wall2 = image_all.subsurface(pygame.rect.Rect(10 * px, 18 * px, px, px))

            self.bomb1 = image_all.subsurface(pygame.rect.Rect(11 * px, 15 * px, px, px))
            self.bomb2 = image_all.subsurface(pygame.rect.Rect(11 * px, 16 * px, px, px))
            self.bomb3 = image_all.subsurface(pygame.rect.Rect(11 * px, 17 * px, px, px))
            
            self.fire_left = image_all.subsurface(pygame.rect.Rect(0 * px, 18 * px, px, px))
            self.fire_horizontal = image_all.subsurface(pygame.rect.Rect(1 * px, 18 * px, px, px))
            self.fire_center = image_all.subsurface(pygame.rect.Rect(2 * px, 18 * px, px, px))
            self.fire_right = image_all.subsurface(pygame.rect.Rect(3 * px, 18 * px, px, px))

            self.fire_up = image_all.subsurface(pygame.rect.Rect(14 * px, 13 * px, px, px))
            self.fire_vertical = image_all.subsurface(pygame.rect.Rect(14 * px, 14 * px, px, px))
            self.fire_down = image_all.subsurface(pygame.rect.Rect(14 * px, 15 * px, px, px))
            
            self.upgrade_bomb = pygame.image.load("resources/upgrade_bomb.png").convert_alpha()
            self.upgrade_bomb = pygame.transform.scale(self.upgrade_bomb, (self.upgrade_bomb.get_width() * 2, self.upgrade_bomb.get_height() * 2))
            self.upgrade_range = pygame.image.load("resources/upgrade_range.png").convert_alpha()
            self.upgrade_range = pygame.transform.scale(self.upgrade_range, (self.upgrade_range.get_width() * 2, self.upgrade_range.get_height() * 2))
            self.upgrade_speed = pygame.image.load("resources/upgrade_speed.png").convert_alpha()
            self.upgrade_speed = pygame.transform.scale(self.upgrade_speed, (self.upgrade_speed.get_width() * 2, self.upgrade_speed.get_height() * 2))

    instance = None

    def getInstance():
        if not Game_Resource.instance:
            Game_Resource.instance = Game_Resource.__Resource()
        return Game_Resource.instance